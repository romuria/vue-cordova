var __library = new __Library()
__library.load("src")
__library.load("page")
__library.load("fragment")
__library.onReady(function () {
    Vue.mixin(__library["src/mixin"])

    window.app = new Vue(__library["src/app"])

    window.app._config = {}

    window.onpopstate = function () {
        window.app.$nav().pop()
    }
    
    window.app.$mount('#main')
    console.log("APP Ready")
})
