var __Library = function (base) {

    // props
    this.__proto__._root = this
    Object.defineProperty(this, "_root", {
        get: function () {
            return this.__proto__._root
        }
    })

    this._module = {}

    // methods
    this.__proto__.load = function (base) {
        if (!base) {
            throw "Library base didn't provide"
            return
        }
        this._module[base] = new __Module(this)
        this._module[base].load(base)
    }

    this.__proto__.onReady = function (callback, error) {
        var _promises = []
        for(var _attr in this._module) {
            _promises.push(this._module[_attr].isReady())
        }
        Promise.all(_promises).then(function () {
            Vue.mixin(this['lib/mixin'])
            for(var comp_id in this['lib/component']) {
                Vue.component(comp_id, this['lib/component'][comp_id])
            }
            for(var dir_id in this['lib/directive']) {
                Vue.directive(dir_id, this['lib/directive'][dir_id])
            }
            document.addEventListener("deviceready", function () {
                if (callback) {
                    callback()
                }
            }, false);
        }.bind(this)).catch(error)
    }
    this.__proto__.require = function (path) {
        if (!path.split('/').length) {
            return Promise.reject(false)
        }
        if (this[path]) {
            return Promise.resolve(this[path])
        } else {
            return Vue.http.get(path + "/__meta.html").then(function (res) {
                return this.asyncRequireDecorator(path, res.bodyText).__originalObj.isReady().then(
                    function (res) {
                        return this.require(path)
                    }.bind(this)
                ).catch(function (err) {
                    return Promise.reject(err)
                }) 
            }.bind(this),
            function (err) {
                return Vue.http.get(path + ".html").then(function (res) {
                    return this.asyncRequireDecorator(path, res.bodyText).__originalObj.isReady().then(
                        function (res) {
                            return this.require(path)
                        }.bind(this)
                    ).catch(function (err) {
                        return Promise.reject(err)
                    }) 
                }.bind(this),
                function (err) {
                    return Promise.reject(err) 
                }.bind(this))
            }.bind(this))
        }
    }
    this.__proto__.asyncRequireDecorator = function (path, html) {
        var _dom = document.createElement('div')
        _dom.innerHTML = html
        _dom = _dom.children[0]
        var _lib = null
        var _path_arr = path.split('/')
        var _popped_path = []
        while (!_lib && _path_arr.length) {
            _popped_path.push(_path_arr.pop())
            _lib = this[_path_arr.join('/')]
        }
        if (!_lib) {
            _path_arr = path.split('/')
            _lib = this._module[_path_arr[0]]
        }
        if (_lib && _path_arr.length == 1) {
            _lib.loadDOM(path, _dom, true)
            return _lib._exports
        }
        if (!_lib && _path_arr.length == 1){
            switch(_dom.tagName) {
                case "MODULE":
                    _lib = this._module[_path_arr[0]] = new __Module(this)
                    _lib.loadDOM(path, _dom)
                    break;
                case "COMPONENT":
                    _lib = this._module[_path_arr[0]] = new __Component(this)
                    _lib.loadDOM(path, _dom)
                    break;
                case "LIB":
                    _lib = this._module[_path_arr[0]] = new __Lib(this)
                    _lib.loadDOM(path, _dom)
                    break;
                case "ICON":
                    _lib = this._module[_path_arr[0]] = new __Icon(this)
                    _lib.loadDOM(path, _dom)
                    break;
            }
            _lib.loadDOM(path, _dom, true)
            return _lib._exports
        }
        if (!_lib &&_path_arr.length > 1) {
            _lib = this._module[_path_arr[0]] = new __Module(this)
        }
        _path_arr = path.split('/').reverse()
        _path_arr.pop()
        while (_path_arr.length > 1) {
            var _base = _path_arr.pop()
            _lib.exports[_base] = new __Module(_lib)
            _lib._exports[_base] = _lib.exports[_base].exports
            _lib = _lib.exports[_base]
            if (_lib._parent != _lib._root) {
                _lib._parent._exports[_base] = _lib._exports
            }
        }
        switch(_dom.tagName) {
            case "MODULE":
                _lib = _lib.exports[_path_arr[0]] = new __Module(_lib)
                _lib.loadDOM(path, _dom)
                break;
            case "COMPONENT":
                _lib = _lib.exports[_path_arr[0]] = new __Component(_lib)
                _lib.loadDOM(path, _dom)
                break;
            case "LIB":
                _lib = _lib.exports[_path_arr[0]] = new __Lib(_lib)
                _lib.loadDOM(path, _dom)
                break;
            case "ICON":
                _lib = this._module[_path_arr[0]] = new __Icon(this)
                _lib.loadDOM(path, _dom)
                break;
        }
        return _lib._exports
    }.bind(this) 

    Vue.http.options.emulateJSON = true;
    this.load('lib')
    
}

var __Module = function (parent) {

    // props
    this._parent = parent
    this._root = this._parent._root
    this._exports = {}
    this.exports = {}

    this._child_promises = []
    this._promise = null
    this._promise_resolve = null
    this._promise_reject = null

    //contructor
    this._promise = new Promise(function (resolve, reject) {
        this._promise_resolve = resolve
        this._promise_reject = reject
    }.bind(this))

    Object.defineProperty(this, "parent", {
        get: function () {
            return this._parent 
        }
    })

    Object.defineProperty(this.exports, "__originalObj", {
        get: function () {
            return this
        }.bind(this)
    })

    Object.defineProperty(this._exports, "__originalObj", {
        get: function () {
            return this
        }.bind(this)
    })
 
    //methods
    this.__proto__.load = function (base, async) {
        Vue.http.get(base + '/__meta.html').then(
            function (res) {
                // parsing html
                var _html = res.bodyText
                var _dom = document.createElement('div')
                _dom.innerHTML = _html
                var _module = _dom.children[0]
                this.loadDOM(base, _module, async) 
            }.bind(this), function (err) {
                this._promise_reject(false)
                console.log(err)
                alert("failed load module from " + base)
            }.bind(this)
        )
    }

    this.__proto__.loadDOM = function (base, _module, async) {
        if (_module.hasAttribute('async') && !async) {
            return
        }     
        for (var i = 0; i < _module.children.length; i++) {
            var _element = _module.children[i]
            if (_element.getAttribute("base")) {
                switch (_element.tagName) {
                    case "LIB":
                        this.exports[_element.getAttribute("base")] = new __Lib(this)
                        break;
                    case "MODULE":
                        this.exports[_element.getAttribute("base")] = new __Module(this)
                        break;
                    case "COMPONENT":
                        this.exports[_element.getAttribute("base")] = new __Component(this)
                        break;
                    case "ICON":
                        this.exports[_element.getAttribute("base")] = new __Icon(this)
                        break;
                }

                if (!_element.children.length && (!_element.hasAttribute('async') || async)) {
                    this.exports[_element.getAttribute("base")].load(base + "/" + _element.getAttribute("base"))
                } else if (_element.hasAttribute('async') && !async) {
                    this.exports[_element.getAttribute("base")]._promise_resolve(true)
                } else {
                    this.exports[_element.getAttribute("base")].loadDOM(_element)
                }
            
                this._child_promises.push(this.exports[_element.getAttribute("base")].isReady())
            }
        }

        this._root[base] = this._exports
        if (this._parent != this._root) {
            this._parent._exports[_module.getAttribute("base").trim()] = this._exports
        }

        this._promise_resolve(true)
    }

    this.__proto__.isReady = function () {
        return this._promise.then(function (res) {
            return Promise.all(this._child_promises).then(function (res) {
                delete this._promise
                delete this._child_promises
                delete this._promise_reject
                delete this._promise_resolve
                return Promise.resolve(true)
            }.bind(this)).catch(function (err) {
                console.log(err)
                return Promise.reject(false)
            })
        }.bind(this)).catch(function (err) {
            console.log(err)
            return Promise.reject(false)
        })
    }
}

var __Lib = function (parent) {

    __Module.bind(this)(parent)

    //custom methods
    this.__proto__.load = function (base) {
        Vue.http.get(base + '.html').then(
            function (res) {
                // parsing html
                var _html = res.bodyText
                var _dom = document.createElement('div')
                _dom.innerHTML = _html
                var _lib = _dom.children[0]
                this.loadDOM(base, _lib)
            }.bind(this), function (err) {
                this._promise_reject(false)
                console.log(err)
                alert("failed load lib from " + base)
            }.bind(this)
        )
    }

    this.__proto__.loadDOM = function (base, _lib) {
        //parsing
        for (var i = 0; i < _lib.children.length; i++) {
            var _element = _lib.children[i]
            switch (_element.tagName) {
                case "SCRIPT":
                    this.exports = eval(_element.innerHTML)
                    break;
            }
        }
        this.exports.__proto__["__identifier"] = (((1+Math.random())*0x10000)|0).toString(16).substring(1) + (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        Object.defineProperty(this.exports, "__originalObj", {
            get: function () {
                return this
            }.bind(this)
        })
        this._exports = this.exports
        this._root[base] = this._exports
        if (this._parent != this._root) {
            this._parent._exports[_lib.getAttribute("base").trim()] = this._exports
        }
        this._promise_resolve(true)
    }

}

var __Component = function (parent) {

    __Module.bind(this)(parent)

    //custom methods
    this.__proto__.load = function (base) {
        Vue.http.get(base + '.html').then(
            function (res) {
                // parsing html
                var _html = res.bodyText
                var _dom = document.createElement('div')
                _dom.innerHTML = _html
                var _comp = _dom.children[0]
                this.loadDOM(base, _comp)
                this._promise_resolve(true)
            }.bind(this), function (err) {
                this._promise_reject(false)
                console.log(err)
                alert("failed load component from " + base)
            }.bind(this)
        )
    }

    this.__proto__.loadDOM = function (base, _comp) {

        this.exports["uid"] = _comp.getAttribute("base")
        this.exports["__identifier"] = (((1+Math.random())*0x10000)|0).toString(16).substring(1) + (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        //parsing
        for (var i = 0; i < _comp.children.length; i++) {
            var _element = _comp.children[i]
            switch (_element.tagName) {
                case "SCRIPT":
                    var _comp_obj  = eval(_element.innerHTML)
                    for (attr in _comp_obj) {
                        this.exports[attr] = _comp_obj[attr]
                    }
                    break;
                case "TEMPLATE":
                    this.exports.template = _element.innerHTML
                    break;
                case "STYLE":
                    this.exports.style = _element.innerHTML
            }
        }
        this._exports = this.exports
        this._root[base] = this._exports
        if (this._parent != this._root) {
            this._parent._exports[_comp.getAttribute("base").trim()] = this._exports
        }
        this._promise_resolve(true)
    }

}

var __Icon = function (parent) {

    __Module.bind(this)(parent)

    //custom methods
    this.__proto__.load = function (base) {
        Vue.http.get(base + '.html').then(
            function (res) {
                // parsing html
                var _html = res.bodyText
                var _dom = document.createElement('div')
                _dom.innerHTML = _html
                var _icon = _dom.children[0]
                this.loadDOM(base, _icon)
                this._promise_resolve(true)
            }.bind(this), function (err) {
                this._promise_reject(false)
                console.log(err)
                alert("failed load component from " + this._src)
            }.bind(this)
        )
    }

    this.__proto__.loadDOM = function (base, _icon) {
        this.exports["__identifier"] = (((1+Math.random())*0x10000)|0).toString(16).substring(1) + (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        this.exports["uid"] = _icon.getAttribute("base")
        this.exports["content"] = _icon.innerHTML
        this._exports = this.exports
        this._root[base] = this._exports
        if (this._parent != this._root) {
            this._parent._exports[_icon.getAttribute("base").trim()] = this._exports
        }
        this._promise_resolve(true)
    }

}