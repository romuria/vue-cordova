const sharp = require("sharp")
const fs = require('fs')

const resources_folder = __dirname + '/../../resources'

if (!fs.existsSync(resources_folder)) {
    fs.mkdirSync(resources_folder)
}

if (!fs.existsSync(resources_folder + '/android')) {
    fs.mkdirSync(resources_folder + '/android')
}

if (!fs.existsSync(resources_folder + '/android/icon')) {
    fs.mkdirSync(resources_folder + '/android/icon')
}

if (!fs.existsSync(resources_folder + '/android/splash')) {
    fs.mkdirSync(resources_folder + '/android/splash')
}

if (!fs.existsSync(resources_folder + '/ios')) {
    fs.mkdirSync(resources_folder + '/ios')
}

if (!fs.existsSync(resources_folder + '/ios/icon')) {
    fs.mkdirSync(resources_folder + '/ios/icon')
}

if (!fs.existsSync(resources_folder + '/ios/splash')) {
    fs.mkdirSync(resources_folder + '/ios/splash')
}

if (!fs.existsSync(resources_folder + '/icon.png')) {
    return console.log('ERROR: icon.png not found in resources folder')
}

if (!fs.existsSync(resources_folder + '/splash.png')) {
    return console.log('ERROR: splash.png not found in resources folder')
}

let inICONStream = resources_folder + '/icon.png';
let inSPLASHStream = resources_folder + '/splash.png';

let imageData = {
    android: {
        icon: {
            folder: fs.realpathSync(resources_folder + '/android/icon'),
            metadata: [
                {
                    title: "drawable-hdpi-icon.png",
                    w: 72,
                    h: 72
                },
                {
                    title: "drawable-ldpi-icon.png",
                    w: 36,
                    h: 36
                },
                {
                    title: "drawable-mdpi-icon.png",
                    w: 48,
                    h: 48,
                },
                {
                    title: "drawable-xhdpi-icon.png",
                    w: 96,
                    h: 96,
                },
                {
                    title: "drawable-xxhdpi-icon.png",
                    w: 144,
                    h: 144,
                },
                {
                    title: "drawable-xxxhdpi-icon.png",
                    w: 192,
                    h: 192,
                }
            ]
        },
        splash: {
            folder: fs.realpathSync(resources_folder + '/android/splash'),
            metadata: [
                {
                    title: "drawable-land-hdpi-screen.png",
                    w: 800,
                    h: 480
                },
                {
                    title: "drawable-land-ldpi-screen.png",
                    w: 320,
                    h: 240
                },
                {
                    title: "drawable-land-mdpi-screen.png",
                    w: 480,
                    h: 320,
                },
                {
                    title: "drawable-land-xhdpi-screen.png",
                    w: 1280,
                    h: 720,
                },
                {
                    title: "drawable-land-xxhdpi-screen.png",
                    w: 1600,
                    h: 960,
                },
                {
                    title: "drawable-land-xxxhdpi-screen.png",
                    w: 1920,
                    h: 1280,
                },
                {
                    title: "drawable-port-hdpi-screen.png",
                    w: 480,
                    h: 800,
                },
                {
                    title: "drawable-port-ldpi-screen.png",
                    w: 240,
                    h: 320,
                },
                {
                    title: "drawable-port-mdpi-screen.png",
                    w: 320,
                    h: 480,
                },
                {
                    title: "drawable-port-xhdpi-screen.png",
                    w: 720,
                    h: 1280,
                },
                {
                    title: "drawable-port-xxhdpi-screen.png",
                    w: 960,
                    h: 1600,
                },
                {
                    title: "drawable-port-xxxhdpi-screen.png",
                    w: 1280,
                    h: 1920,
                }
            ]
        }
    },
    ios: {
        icon: {
            folder: fs.realpathSync(resources_folder + '/ios/icon'),
            metadata: [
                {
                    title: "icon.png",
                    w: 57,
                    h: 57
                },
                {
                    title: "icon-40.png",
                    w: 40,
                    h: 40
                },
                {
                    title: "icon-40@2x.png",
                    w: 80,
                    h: 80,
                },
                {
                    title: "icon-40@3x.png",
                    w: 120,
                    h: 120,
                },
                {
                    title: "icon-50.png",
                    w: 50,
                    h: 50,
                },
                {
                    title: "icon-50@2x.png",
                    w: 100,
                    h: 100,
                },
                {
                    title: "icon-60.png",
                    w: 60,
                    h: 60,
                },
                {
                    title: "icon-60@2x.png",
                    w: 120,
                    h: 120,
                },
                {
                    title: "icon-60@3x.png",
                    w: 180,
                    h: 180,
                },
                {
                    title: "icon-70.png",
                    w: 70,
                    h: 70,
                },
                {
                    title: "icon-70@2x.png",
                    w: 140,
                    h: 140,
                },
                {
                    title: "icon-76.png",
                    w: 76,
                    h: 76,
                },
                {
                    title: "icon-76@2x.png",
                    w: 152,
                    h: 152,
                },
                {
                    title: "icon-83.5@2x.png",
                    w: 167,
                    h: 167,
                },
                {
                    title: "icon-1024.png",
                    w: 1024,
                    h: 1024,
                },
                {
                    title: "icon@2x.png",
                    w: 114,
                    h: 114,
                },
                {
                    title: "icon-small.png",
                    w: 29,
                    h: 29,
                },
                {
                    title: "icon-small@2x.png",
                    w: 58,
                    h: 58,
                },
                {
                    title: "icon-small@3x.png",
                    w: 87,
                    h: 87,
                }
            ]
        },
        splash: {
            folder: fs.realpathSync(resources_folder + '/ios/splash'),
            metadata: [
                {
                    title: "Default-568h@2x~iphone.png",
                    w: 640,
                    h: 1136
                },
                {
                    title: "Default-667h.png",
                    w: 750,
                    h: 1334
                },
                {
                    title: "Default-736h.png",
                    w: 1242,
                    h: 2208,
                },
                {
                    title: "Default@2x~iphone.png",
                    w: 640,
                    h: 960,
                },
                {
                    title: "Default@2x~universal~anyany.png",
                    w: 2732,
                    h: 2732,
                },
                {
                    title: "Default~iphone.png",
                    w: 320,
                    h: 480,
                },
                {
                    title: "Default-Landscape-736h.png",
                    w: 2208,
                    h: 1242,
                },
                {
                    title: "Default-Landscape@2x~ipad.png",
                    w: 2048,
                    h: 1536,
                },
                {
                    title: "Default-Landscape~ipad.png",
                    w: 1024,
                    h: 768,
                },
                {
                    title: "Default-Landscape@~ipadpro.png",
                    w: 2732,
                    h: 2048,
                },
                {
                    title: "Default-Portrait@2x~ipad.png",
                    w: 1536,
                    h: 2048,
                },
                {
                    title: "Default-Portrait~ipad.png",
                    w: 768,
                    h: 1024,
                },
                {
                    title: "Default-Portrait@~ipadpro.png",
                    w: 2048,
                    h: 2732,
                }
            ]
        }
    }
}

let resizeImage = function (in_stream, out_dir, metadata) {
    return new Promise(function(resolve, reject) {
        sharp(in_stream)
            .resize({ 
                width: metadata.w, 
                height: metadata.h, 
                fit: sharp.fit.cover,
                position: sharp.gravity.center
            })
            .toFile(out_dir + '/' + metadata.title)
            .then(() => {resolve(metadata.title + " (" + metadata.w + " x " + metadata.h + ") => [DONE]\n")})
            .catch((err) => {resolve(metadata.title + " (" + metadata.w + " x " + metadata.h + ") => [ERROR]\n")})
    })
}

console.log("----------GENERATING RESOURCES----------")

let promises = []

imageData.android.icon.metadata.forEach((metadata) => {
    promises.push(resizeImage(inICONStream, imageData.android.icon.folder, metadata))
})

imageData.android.splash.metadata.forEach((metadata) => {
    promises.push(resizeImage(inSPLASHStream, imageData.android.splash.folder, metadata))
})

imageData.ios.icon.metadata.forEach((metadata) => {
    promises.push(resizeImage(inICONStream, imageData.ios.icon.folder, metadata))
})

imageData.ios.splash.metadata.forEach((metadata) => {
    promises.push(resizeImage(inSPLASHStream, imageData.ios.splash.folder, metadata))
})

Promise.all(promises).then((res) => {

    res.forEach((item) => console.log(item))

    console.log('[DONE GENERATE RESOURCES]')

}).catch((res) => {

    res.forEach((item) => console.log(item))

    console.log('[THERE IS SOME ERROR WHILE GENERATE RESOURCES]')
    
})
// const icon_src = fs.createReadStream('img.jpg');