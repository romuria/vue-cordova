const cp = require('child_process');
const fs = require('fs')
const jsdom = require("jsdom")

if (process.argv.length < 3) {
    return console.log('Please specify plugin name')
}

var plugin_name = process.argv[2]

const src = fs.realpathSync(__dirname + '/src/plugin')
const root = fs.realpathSync(__dirname + '../../..')
const plugin_dir = fs.realpathSync(root + '/www/lib/native')

if (fs.readdirSync(plugin_dir).indexOf(plugin_name + '.html') < 0) {
    return console.log('Plugin \"' + plugin_name + '\" not exist in you project')
}

var plugin_content = fs.readFileSync(plugin_dir + '/' + plugin_name + '.html').toString()

var plugin_html = new jsdom.JSDOM()
plugin_html.window.document.getElementsByTagName('body')[0].innerHTML = plugin_content

var cordova_plugin_name = plugin_html.window.document.getElementsByTagName('cordova-plugin-name')[0].textContent.trim()

try {
    console.log(cp.execSync("cordova plugin rm " + cordova_plugin_name, {cwd: root}).toString())
    fs.unlinkSync(plugin_dir + '/' + plugin_name + '.html')

    var meta = new jsdom.JSDOM()
    meta.window.document.getElementsByTagName('body')[0].innerHTML = fs.readFileSync(plugin_dir + '/__meta.html')
    var plugin_list = meta.window.document.getElementsByTagName('lib')
    for (var _i = 0; _i < plugin_list.length; _i ++) {
        if (plugin_list[_i].getAttribute("base") == plugin_name) {
            plugin_list[_i].remove()
        }
    }
    fs.writeFileSync(plugin_dir + '/__meta.html', meta.window.document.getElementsByTagName('module')[0].outerHTML)

} catch(err) {
    console.log(err)
    console.log("\n\nFAILED ADD PLUGIN")
}