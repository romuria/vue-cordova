const cp = require('child_process');
const fs = require('fs')

const root = fs.realpathSync(__dirname + '../../..')

if (!fs.existsSync(root + '/.vue-cordova')) {
    return console.log('Please run \"npm run initsync\" before sync project')
}

cp.execSync("git pull", {cwd: root + '/.vue-cordova'})

let changed_files = cp.execSync("git diff --name-only " + fs.readFileSync(root + '/.vue-cordova/.last_commit').toString(), {cwd:root + '/.vue-cordova'}).toString().split("\n")

changed_files.forEach((path) => {
    if (!path.trim().length) {
        return
    }
    try {
        let split_path = path.split("/")
        for (let i = 0; i < split_path.length - 1; i++) {
            if (!fs.existsSync(root + "/" + split_path.slice(0, i + 1).join('/'))) {
                fs.mkdirSync(root + "/" + split_path.slice(0, i + 1).join('/'))
            }
        }
        if (!fs.existsSync(root + '/.vue-cordova' + "/" + path)) {
            fs.unlinkSync(root + "/" + path)
        } else {
            fs.writeFileSync(root + "/" + path, fs.readFileSync(root + '/.vue-cordova' + "/" + path))
        }
    } catch (err) {
        console.log(err)
    }
})

fs.writeFileSync(fs.realpathSync(root + '/.vue-cordova') + '/.last_commit', cp.execSync("git log --pretty=oneline", {cwd: root + '/.vue-cordova'}).toString().split(' ')[0])

console.log("DONE")