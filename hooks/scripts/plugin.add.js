const cp = require('child_process');
const fs = require('fs')
const jsdom = require("jsdom")

if (process.argv.length < 3) {
    return console.log('Please specify plugin name')
}

var plugin_name = process.argv[2]

const src = fs.realpathSync(__dirname + '/src/plugin')
const root = fs.realpathSync(__dirname + '../../..')
const plugin_dir = fs.realpathSync(root + '/www/lib/native')
const plugin_list  = fs.readdirSync(src)

plugin_list.forEach((item, index) =>  {
    plugin_list[index] = item.split(".")[0]
})

if (fs.readdirSync(plugin_dir).indexOf(plugin_name + '.html') >= 0) {
    return console.log('Plugin \"' + plugin_name + '\" already exist in you project')
}

if (plugin_list.indexOf(plugin_name) < 0) {
    return console.log('plugin ' + '\"' + plugin_name + '\"' + ' not exist')
}

var plugin_content = fs.readFileSync(src + '/' + plugin_name + '.html').toString()

var plugin_html = new jsdom.JSDOM()
plugin_html.window.document.getElementsByTagName('body')[0].innerHTML = plugin_content

var cordova_plugin_name = plugin_html.window.document.getElementsByTagName('cordova-plugin-name')[0].textContent.trim()

try {
    console.log(cp.execSync("cordova plugin add " + cordova_plugin_name, {cwd: root}).toString())
    fs.writeFileSync(plugin_dir + '/' + plugin_name + '.html', plugin_content)

    var meta = new jsdom.JSDOM()
    meta.window.document.getElementsByTagName('body')[0].innerHTML = fs.readFileSync(plugin_dir + '/__meta.html')
    var plugin_meta = meta.window.document.createElement('lib')
    plugin_meta.setAttribute('base', plugin_name)

    meta.window.document.getElementsByTagName('module')[0].append(plugin_meta)
    fs.writeFileSync(plugin_dir + '/__meta.html', meta.window.document.getElementsByTagName('module')[0].outerHTML)

} catch(err) {
    console.log(err)
    console.log("\n\nFAILED ADD PLUGIN")
}