const cp = require('child_process');
const fs = require('fs')

const root = fs.realpathSync(__dirname + '../../..')

if (!fs.existsSync(root + '/.vue-cordova')) {
    cp.execSync("git clone git@bitbucket.org:romuria/vue-cordova.git .vue-cordova", {cwd: root})
}

fs.writeFileSync(fs.realpathSync(root + '/.vue-cordova') + '/.last_commit', cp.execSync("git log --pretty=oneline", {cwd: root + '/.vue-cordova'}).toString().split(' ')[0])
fs.appendFileSync(fs.realpathSync(root + '/.vue-cordova') + '/.gitignore', '.last_commit')

cp.execSync("git remote remove vue-cordova", {cwd: root})

console.log("DONE")