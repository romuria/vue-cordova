const cp = require('child_process');
const fs = require('fs')
const jsdom = require("jsdom")

console.log("[COMPILING SCSS]")

fs.appendFileSync("platforms/android/assets/www/css/style.scss", "@import \"./android.scss\";")
appendComponentSassHTML("platforms/android/assets/www")

cp.exec([
    "sass platforms/android/assets/www/css/style.scss:platforms/android/assets/www/css/style.css",
    "autoprefixer-cli -b \"last 10 version\" platforms/android/assets/www/css/style.css"
].join(" && "), function (error, stdout, stderr) {
    if (error) {
        console.log("ERROR : ", error)
        return
    }
    
    console.log(stdout)
    console.log(stderr)
    console.log("DONE")
    console.log("[REMOVING *.sass FILES]")
    
    cp.exec([
        "rm platforms/android/assets/www/css/style.scss",
        "rm platforms/android/assets/www/css/browser.scss",
        "rm platforms/android/assets/www/css/android.scss",
        "rm platforms/android/assets/www/css/ios.scss",
        "rm platforms/android/assets/www/css/_directives.scss",
        "rm -R platforms/android/assets/www/css/_style"
	].join(" && "), function (error, stdout, stderr) {
        if (error) {
            console.log("ERROR : ", error)
            return
        }
        console.log(stdout)
        console.log(stderr)
        
        console.log("DONE")
    }) 
})

function appendComponentSassHTML(path_src) {
    if (!fs.existsSync(path_src)) {
        return
    }
    if (!fs.statSync(path_src).isDirectory()) {
        var file_html = new jsdom.JSDOM()
        file_html.window.document.getElementsByTagName('body')[0].innerHTML = fs.readFileSync(path_src).toString()
        if (!file_html.window.document.getElementsByTagName('component').length) {
            return
        }
        var component_element = file_html.window.document.getElementsByTagName('component')[0]
        var sass_el = component_element.getElementsByTagName('sass')
        if (!sass_el.length) {
            return
        }
        console.log(path_src)
        sass_el = sass_el[0]
        sass_el.remove()
        sass_str = ('\n' + sass_el.innerHTML.trim() + '\n').split("&amp;").join("&").split("&gt;").join(">")
        fs.appendFileSync("platforms/android/assets/www/css/style.scss", sass_str)
        fs.writeFileSync(path_src, component_element.outerHTML)
        return
    } else {
        fs.readdirSync(path_src).forEach(function (dir) {
            appendComponentSassHTML(path_src + '/' + dir)
        })
    }

}
