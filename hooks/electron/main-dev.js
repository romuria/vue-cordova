const { app, BrowserWindow } = require('electron')

let mainWindow

function createWindow() {
    mainWindow = new BrowserWindow({ width: 800, height: 600})
    mainWindow.setMenuBarVisibility(false)
    mainWindow.maximize()
    mainWindow.loadFile(__dirname + '/www/index.html')
    mainWindow.on('closed', function () {
        mainWindow = null
    })
}

app.on('ready', createWindow)
app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', function () {
    if (mainWindow === null) {
        createWindow()
    }
})

// this is just dev func
const net = require('net');
server = net.createServer((srv) => {
    srv.on("data", (data) => {
        console.log(data.toString())
        if (data.toString() == 'reload-app') {
            mainWindow.reload()
        }
    })
});
server.listen(__dirname + '/electron.dev.sock', null, (ev) => {
    console.log(server.address())
})