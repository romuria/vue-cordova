var watchr = require('watchr')
const cp = require('child_process');
const fs = require('fs')

const root = fs.realpathSync(__dirname + '/../..');
const target = fs.realpathSync(__dirname + '/../../electron')

var path = root + '/www'
let app_process= null

function listener (changeType, fullPath, currentStat, previousStat) {
    onChange()
	switch ( changeType ) {
		case 'update':
			console.log('the file', fullPath, 'was updated')
			break
		case 'create':
			console.log('the file', fullPath, 'was created')
			break
		case 'delete':
			console.log('the file', fullPath, 'was deleted')
			break
	}
}
function next (err) {
	if ( err )  return console.log('watch failed on', path, 'with error', err)
	console.log('watch successful on', path)
}

function onChange() {
    console.log(cp.execSync("cd " + root + " && cordova prepare browser"))
    try {
        cp.execSync("rm -R " + target + "/www")
    } catch (err) {

    }
    cp.execSync("cp -R " + target + '/../platforms/browser/www ' + target + '/www')
    app_process.write('reload-app')
}

console.log("[STARTING APP]")
cp.exec("./node_modules/electron/dist/electron main-dev.js", {cwd: target})
var int_id = setInterval(()=>{
    if (!fs.existsSync(target + '/electron.dev.sock')) {
        return
    }
    clearInterval(int_id)
    watchr.open(path, listener, next)
    app_process = require('net').createConnection(target + '/electron.dev.sock')
    onChange()
}, 500)