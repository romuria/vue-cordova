const root = __dirname + '/../..';
const fs = require('fs');
const cp = require('child_process');

const target = __dirname + '/../../electron'

const create = function () {
    if (fs.existsSync(target)) {
        console.log("Electron platform already exist")
        return
    }

    fs.mkdirSync(target)

    try {
        var main_package_json = JSON.parse(fs.readFileSync(root + '/package.json', 'utf8'));
        delete main_package_json.scripts
        delete main_package_json.cordova
        main_package_json.devDependencies = {"electron": "^4.0.5"}
        main_package_json.dependencies = {}
        main_package_json.main = "main.js"

        fs.writeFileSync(target + '/package.json', JSON.stringify(main_package_json, null, 4))
        var npm_process = cp.exec([
            "cd " + target,
            "npm install"
        ].join(" && "), function (error, stdout, stderr) {
            if (error) {
                console.log("ERROR : ", error)
                return
            }
            console.log(stdout)
            console.log(stderr)
            fs.copyFileSync(__dirname + '/main.js', target + '/main.js')
            fs.copyFileSync(__dirname + '/main-dev.js', target + '/main-dev.js')
            require('fs-extra').copy(target + '/../platforms/browser/www', target + '/www', function (err) {
                if (err) {
                    console.log(err)
                    return
                }
                console.log('[ELECTRON PLATFORM ADDED]')
            })
        })
    } catch(err) {
        console.log("[Failed create project!]")
        console.log(err)
        fs.rmdirSync(target)
    }

}

create()
