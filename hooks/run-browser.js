var watchr = require('watchr')
const exec = require('child_process').exec;

var path = process.cwd() + '/www'
function listener (changeType, fullPath, currentStat, previousStat) {
    exec("cordova prepare browser")
	switch ( changeType ) {
		case 'update':
			console.log('the file', fullPath, 'was updated')
			break
		case 'create':
			console.log('the file', fullPath, 'was created')
			break
		case 'delete':
			console.log('the file', fullPath, 'was deleted')
			break
	}
}
function next (err) {
	if ( err )  return console.log('watch failed on', path, 'with error', err)
	console.log('watch successful on', path)
}

console.log("[STARTING DEV SERVER]")
exec("cordova run browser")

watchr.open(path, listener, next)